
def is_meaningful(sentence, free_var=[]):
    if '(' not in sentence:
        equality = False
        for i in range(len(sentence)):
            if i%2 == 1:
                if sentence[i][0] not in ['+','=']:
                    print('1=', sentence)
                    return False
                if sentence[i][0] == '=' and equality == True:
                    print('2=', sentence)
                    return False
                elif sentence[i][0] == '=':
                    equality = True
            elif sentence[i] not in (free_var + ['0','1']):
                print('3=', sentence)
                return False
        return True
    bra = 0
    constants = ['~','(',')','&',':','|','A','E','+','=','0','1']
    open_bra = []
    close_bra = []
    for i in range(len(sentence)):
        if sentence[i] == '(':
            bra += 1
            if bra == 1:
                open_bra.append(i)
        elif sentence[i] == ')':
            bra -= 1
            if bra == 0:
                close_bra.append(i)
        if bra == 0 and sentence[i].islower() and sentence[i].isalpha() and sentence[i] not in free_var:
            if i < len(sentence)-1:
                if sentence[i+1] not in ['E','A']:
                    print('17=', sentence)
                    return False
            else:
                print('4=', sentence)
                return False
        if bra < 0:
            print('5=', sentence)
            return False
    if bra != 0:
        print('6=', sentence)
        return False
    for i in range(len(open_bra)):
        start = open_bra[i]
        end = close_bra[i]
        recursive_check = False
        if start > 0:
            if sentence[start-1] in ['A','E']:
                if start > 1:
                    ivar = sentence[start - 2]
                    if ivar in free_var or not ivar.isalpha() or not ivar.islower():
                        print('7=', sentence)
                        return False
                    new_var = free_var + [ivar]
                    open_bra[i] = start - 2
                    recursive_check = True
                    if not is_meaningful(sentence[start+1:end],new_var):
                        print('8=', sentence)
                        return False
                else:
                    print('9=',start, sentence)
                    return False
        if not recursive_check:
            if not is_meaningful(sentence[start+1:end],free_var):
                print('10=', sentence)
                return False
        
    start = open_bra[0]
    if start == 1:
        if sentence[0] != '~':
            print('11=', sentence)
            return False
    elif start != 0:
        print('12=', sentence)
        return False
    operator = ''
    for i in range(len(open_bra)-1):
        end_ = close_bra[i]
        start_ = open_bra[i+1]
        if start_ - end_ == 2:
            if sentence[end_+1] not in ['|','&',':'] or (operator != '' and operator != sentence[end_+1]):
                print('13=', sentence)
                return False
            operator = sentence[end_+1]
        elif start_ - end_ == 3:
            if sentence[end_+1] not in ['|','&',':'] or sentence[end_+2] != '~' or (operator != '' and operator != sentence[end_+1]):
                print('14=', sentence)
                return False
            operator = sentence[end_+1]
        else:
            print('15=', start_,end_, sentence)
            return False
    #print('16=', sentence)
    return close_bra[-1] == len(sentence)-1

def eliminate_implication(sentence):
    while True:
        index = -1
        for i in range(len(sentence)):
            if sentence[i] == ':':
                index = i
        if index == -1:
            return sentence
        end1 = index
        start2 = index+1
        bra = 0
        flag = False
        for i in range(index,len(sentence)):
            if sentence[i] == '(':
                bra += 1
                flag = True
            elif sentence[i] == ')':
                bra -= 1
            if bra == 0 and flag:
                end2 = i+1

        flag = False
        bra = 0
        for i in range(index-1,-1,-1):
            if sentence[i] == ')':
                bra += 1
                flag = True
            elif sentence[i] == '(':
                bra -= 1
            if bra == 0 and flag:
                start1 = i
                
        first = sentence[start1:end1]
        first_prime = first.copy()
        for i in range(len(first_prime)):
            if first_prime[i][0].isalpha() and first_prime[i][0].islower():
                first_prime[i] += "'"
        second = sentence[start2:end2]
        sentence = sentence[:start1] + ['~'] + first_prime + ['|','('] + first + ['&'] + second + [')']
    

def free_variables(sentence,start_,end_,recursive=True):
    quantifier = -1
    free_var = dict()
    bra = 0
    end = -1
    variable = ''
    for i in range(start_,end_):
        if quantifier == -1 and (sentence[i] == 'A' or sentence[i] == 'E'):
            variable = sentence[i-1]
            start = i+1
            quantifier = bra
            continue
        if sentence[i] in ['(','{']:
            bra += 1
        elif sentence[i] in [')','}']:
            bra -= 1
        if bra == quantifier:
            end = i+1
            quantifier = -1
            if recursive:
                nested_var = free_variables(sentence,start+1,i)
                free_var[variable] = (start,end,nested_var)
            else:
                free_var[variable] = (start,end)
    return free_var


def other_quant(quant):
    if quant == 'E':
        return 'A'
    elif quant == 'A':
        return 'E'

def eliminate_neg_quantifiers(sentence):
    variables = free_variables(sentence,0,len(sentence),False)
    quantifiers = sorted([(pos,var) for var,pos in variables.items()])[::-1]
    caught = False
    for pos,var in quantifiers:
        start,end = pos
        if start >= 3:
            if sentence[start-3]=='~':
                caught = True
                nested, _ = eliminate_neg_quantifiers(sentence[start:end])
                sentence = sentence[:start-3] + [var] + [other_quant(sentence[start-1])] + ['{','~','('] + nested[1:-1] + [')','}'] + sentence[end:]
                continue
        nested, excaught = eliminate_neg_quantifiers(sentence[start:end])
        sentence = sentence[:start] + nested + sentence[end:]
        if excaught:
            caught = True
    return sentence, caught

def change_braces(sentence,variables):
    for var,pos in variables.items():
        start,end,nested = pos
        sentence[start] = '{'
        sentence[end-1] = '}'
        sentence = change_braces(sentence,nested)
    return sentence

def bracing(sentence):
    i = 0
    while i < len(sentence):
        if sentence[i] == '{':
            sentence = sentence[:i] + ['{','('] + sentence[i+1:]
            i += 1
        if sentence[i] == '}':
            sentence = sentence[:i] + [')','}'] + sentence[i+1:]
            i += 1
        i += 1
    return sentence

def bracket_opener(sentence):
    max_bra = 0
    start = -1
    end = len(sentence)+1
    bra = 0
    for i in range(0,len(sentence)):
        if sentence[i] == '(':
            bra += 1
            if max_bra <= bra:
                max_bra = bra
                start = i
        elif sentence[i] == ')':
            if bra == max_bra:
                end = i+1
            bra -= 1

    flag = False
    for i in range(start,end-1):
        if sentence[i] in ['E','A']:
            flag = True
    if flag == False:
        sentence[start] = '{'
        sentence[end-1] = '}'
        return sentence

    q_start = []
    q_end = []
    for i in range(start+1,end-1):
        if sentence[i] in ['E','A']:
            q_start = q_start + sentence[i-1:i+1] + ['{']
            q_end = ['}'] + q_end
    sub_sentence = sentence[start+1:end-1]
    i = 0
    while i < len(sub_sentence):
        if sub_sentence[i] in ['E','A']:
            sub_sentence = sub_sentence[:i-1] + sub_sentence[i+1:]
            i -= 2
        i += 1
    if start == -1:
        return q_start + sub_sentence + q_end
    sentence = sentence[:start] + q_start + sub_sentence + q_end + sentence[end:]
    return sentence


def eliminate_superflous_brackets(sentence):
    bra = 0
    open_bra = []
    close_bra = []
    stack = []
    for i in range(len(sentence)):
        if sentence[i] == '{':
            bra += 1
            stack.append(i)
        elif sentence[i] == '}':
            bra -= 1
            open_bra.append(stack.pop())
            close_bra.append(i)
    deletions = []
    for i in range(len(open_bra)-1):
        if close_bra[i+1] == close_bra[i]+1 and open_bra[i+1]+1 == open_bra[i]:
            deletions = deletions + [open_bra[i],close_bra[i]]
    new_sentence = []
    for i in range(len(sentence)):
        if i not in deletions:
            new_sentence.append(sentence[i])
    return new_sentence
        
        

def all_quantifiers_on_left(sentence):
    index = 1
    for i in range(1,len(sentence),3):
        index = i
        if sentence[i] not in ['A','E']:
            break
    for i in range(index, len(sentence)):
        if sentence[i] in ['A','E']:
            return False
    return True   

def all_round_brackets_removed(sentence):
    if '(' not in sentence:
        return True
    return False

def matrix(sentence):
    index = -2
    quans = []
    variables = []
    for i in range(1,len(sentence),3):
        if sentence[i] not in ['A','E']:
            break
        quans.append(sentence[i])
        variables.append(sentence[i-1])
        index = i
    M = sentence[index+2:len(sentence) - (index+2)//3]
    carry_neg = False
    for i in range(len(quans)):
        if quans[i] == 'A' and not carry_neg:
            quans[i] = '~E'
            carry_neg = True
        elif quans[i] == 'E' and carry_neg:
            quans[i] = '~E'
            carry_neg = False
        elif quans[i] == 'A' and carry_neg:
            quans[i] = 'E'
            carry_neg = True
    if carry_neg:
        M = ['~','{'] + M + ['}']
    return M, quans, variables

import random

def random_sentence(var = [], declared = [], depth = 0):
    r = 2
    if random.randint(0,depth) == 0:
        r = 3
    if len(var) == 0:
        coin = random.randint(1,r)
    else:
        coin = random.randint(0,r)
    if coin <= 0:
        eq = ['(']
        var = var + ['1']
        for v in var:
            coeff = abs(1+int(random.gauss()))
            for j in range(coeff):
                eq = eq + [v] + ['+']
        eq = eq +['0','=']
        for v in var:
            coeff = abs(int(random.gauss()))
            for j in range(coeff):
                eq = eq + [v] + ['+']
        return eq + ['0',')'], declared
    elif coin == 1:
        new_var = chr(ord('a') + len(declared))
        coin = random.randint(0,1)
        if coin == 0:
            quan = [new_var,'E','(']
        else:
            quan = [new_var,'A','(']
        sentence, new_declared = random_sentence(var + [new_var], declared + [new_var], depth)
        return quan + sentence + [')'], new_declared
    elif coin == 2:
        sentence, new_declared = random_sentence(var,declared, depth)
        return ['~','('] + sentence + [')'], new_declared
    else:
        count = 1 + abs(int(random.gauss()/(depth+1)))
        sentence, new_declared = random_sentence(var, declared, depth+count)
        operator = random.choice(['|','&'])
        for i in range(count):
            new_sentence, new_declared = random_sentence(var, new_declared, depth+count)
            sentence = sentence + [operator] + new_sentence
        return ['('] + sentence + [')'], new_declared
        

def eval_bool(sentence):
    sentence = ''.join(sentence)
    while len(sentence) > 1:
        #print(sentence)
        k = sentence.find('{F}')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('{T}')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
            
        k = sentence.find('~F')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+2:]
        k = sentence.find('~T')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+2:]
            
        k = sentence.find('F&T')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('T&F')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('F&F')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('T&T')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
            
            
        k = sentence.find('F|T')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
        k = sentence.find('T|F')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
        k = sentence.find('F|F')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('T|T')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
    return sentence[0]

def combinations(n):
    #if n > 10:
    #    print('overflow 2^',n)
    #    exit()
    if n == 0:
        return [[]]
    else:
        ilist = []
        for i in combinations(n-1):
            ilist = ilist + [i+['F'],i+['T']]
        return ilist

def get_conjuncts(matrix):
    pos = []
    eqs = []
    i = 0
    while i < len(matrix):
        #print(matrix)
        if matrix[i] in ['0','1'] or matrix[i].islower():
            pos.append(i)
            end = i
            for j in range(i,len(matrix)):
                end = j
                if matrix[j] == '}':
                    break
                if j == len(matrix)-1:
                    j = len(matrix)
            eqs = eqs + [matrix[i:j]]
            matrix = matrix[:i+1] + matrix[j:]
            matrix[i] = 'V'
        i += 1
    return matrix, pos, eqs

def equation(sentence,var):
    coeff = [0 for v in range(len(var)+1)]
    mod = None
    for v in range(len(var)):
        sign = 1
        for i in range(len(sentence)):
            if sentence[i] == var[v]:
                coeff[v] += sign
            elif sentence[i][0] == '=':
                sign = -1
                if len(sentence[i]) == 1:
                    mod = 0
                else:
                    mod = int(sentence[i][1:])
    sign = 1
    for i in range(len(sentence)):
        if sentence[i] == '1':
            coeff[-1] += sign
        if sentence[i][0] == '=':
            sign = -1
            if len(sentence[i]) == 1:
                mod = 0
            else:
                mod = int(sentence[i][1:])
    return coeff,mod

def disjunctive_normal_form(matrix):
    matrix,pos,eqs = get_conjuncts(matrix)
    n = len(pos)
    result = []


    same = dict()
    for e in range(len(eqs)):
        u = tuple(eqs[e])
        same.setdefault(u,[]).append(pos[e])
    n = len(same.keys())

    
    for truths in combinations(n):
        m = matrix
        #for b in range(n):
        #    m[pos[b]] = truths[b]

        ####
        pos2 = list(same.values())
        for b in range(len(pos2)):
            for bb in pos2[b]:
                m[bb] = truths[b]
        ####
                  
        ans = eval_bool(m)
        if ans == 'T':
            result.append(truths)
    print(result)
    #return result,pos,eqs
    return result,pos,[list(v) for v in same.keys()]

def compute_gcd(x, y):
    if x == 0:
        return y
    if y == 0:
        return x
    if x == 1 or y == 1:
        return 1
    if x>y:
        return compute_gcd(x%y,y)
    return compute_gcd(x,y%x)
    
def compute_lcm(x, y):

   # choose the greater number
   if x > y:
       greater = x
   else:
       greater = y

   while(True):
       if((greater % x == 0) and (greater % y == 0)):
           lcm = greater
           break
       greater += 1

   return lcm

def sgn(n):
    if n > 0:
        return 1
    elif n < 0:
        return -1
    else:
        print('sign of zero')
        return None

def solve_two_congruences(v,eqj,modj,eqk,modk):
    onej = sgn(eqj[v])
    onek = sgn(eqk[v])
    lcm = compute_lcm(modj,modk)
    for i in range(len(eqj)):
        eqj[i] = onej * eqj[i] * abs(lcm//modj)
        eqk[i] = onek * eqk[i] * abs(lcm//modk)
    modj = lcm
    modk = lcm
    while eqj[v] != eqk[v]:
        if eqj[v] > eqk[v]:
            for i in range(len(eqj)):
                eqj[i] = eqj[i] - eqk[i]
        else:
            for i in range(len(eqj)):
                eqk[i] = eqk[i] - eqj[i]
    for i in range(len(eqj)):
        eqk[i] = eqk[i] - eqj[i]
    return eqj,modj,eqk,modk


def find_list(sentence,sub):
    sub = [i for i in sub]
    for j in range(len(sentence) - len(sub) + 1):
        if all([sentence[j+k] == sub[k] for k in range(len(sub))]):
            return j
    return -1
        

def eval_matrix(sentence):
    while len(sentence) > 1 and ('T' in sentence or 'F' in sentence):
        print(''.join(sentence))
        k = find_list(sentence, '{F}')
        if k!=-1:
            sentence = sentence[:k] + ['F'] + sentence[k+3:]
        k = find_list(sentence, '{T}')
        if k!=-1:
            sentence = sentence[:k] + ['T'] + sentence[k+3:]

        caught = True
        while caught:
            caught = False
            k = find_list(sentence, '~F')
            if k!=-1:
                caught = True
                sentence = sentence[:k] + ['T'] + sentence[k+2:]
            k = find_list(sentence, '~T')
            if k!=-1:
                caught = True
                sentence = sentence[:k] + ['F'] + sentence[k+2:]
            
        k = find_list(sentence, 'F&')
        if k!=-1:
            if sentence[k+2] in ['T','F']:
                j = k+2
            else:
                bracket = 0
                i = k+2
                if sentence[k+2] == '~':
                    i=k+3
                for s in range(i,len(sentence)):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
            sentence = sentence[:k] + ['F'] + sentence[j+1:]
            
        k = find_list(sentence, '&F')
        if k!=-1:
            if sentence[k-1] in ['T','F']:
                j = k-1
            else:
                bracket = 0
                for s in range(k-1,-1,-1):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
                if j>0:
                    if sentence[j-1] == '~':
                        j -= 1
            sentence = sentence[:j] + ['F'] + sentence[k+2:]
            
        k = find_list(sentence, '&T')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
        k = find_list(sentence, 'T&')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
            

            
        k = find_list(sentence, 'T|')
        if k!=-1:
            if sentence[k+2] in ['T','F']:
                j = k+2
            else:
                bracket = 0
                i = k+2
                if sentence[k+2] == '~':
                    i=k+3
                for s in range(i,len(sentence)):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
            sentence = sentence[:k] + ['T'] + sentence[j+1:]
            
        k = find_list(sentence, '|T')
        if k!=-1:
            if sentence[k-1] in ['T','F']:
                j = k-1
            else:
                bracket = 0
                for s in range(k-1,-1,-1):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
                if j>0:
                    if sentence[j-1] == '~':
                        j -= 1
            sentence = sentence[:j] + ['T'] + sentence[k+2:]
            
        k = find_list(sentence, '|F')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
        k = find_list(sentence, 'F|')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
            
    return sentence


def eliminate_last_variable(eqs, result,mod, quan):
    if len(result) == 0:
        if len(quan) > 0:
            if quan[-1] == '~E':
                return ['0','=0','0'],quan[:-1],[chr(ord('a')+v) for v in range(len(quan)-1)]
            else:
                return ['1','=0','0'],quan[:-1],[chr(ord('a')+v) for v in range(len(quan)-1)]
        else:
            return ['1','=0','0'], [], []
        
    #print(eqs, result,mod, quan)
    i = len(quan)-1 # last variable
    new_eqs = [[e.copy() for e in eqs] for p in range(len(result))]
    new_mod = [mod.copy() for p in range(len(result))]
    #print(new_eqs,new_mod,result)

    print('yes neg cong',new_eqs,new_mod,result)
    #print()
    
    p = 0
    while p < len(result):
        for r in range(len(result[p])):
            if result[p][r] == 'F' and new_mod[p][r]!=0:

                result[p][r] = 'T'
                for m in range(1,new_mod[p][r]):    
                    false_set = [e.copy() for e in new_eqs[p]]
                    false_set[r][-1] += m
                    false_set[r][-1] = false_set[r][-1]%new_mod[p][r]

                    new_eqs = new_eqs + [false_set]
                    new_mod = new_mod + [new_mod[p].copy()]
                    result = result + [result[p].copy()]
                    
                if new_mod[p][r] == 1:
                    new_eqs[p][r] = [0 for i in new_eqs[p][r]]
                    new_eqs[p][r][-1] = 1
                    new_mod[p][r] = 0
                    result[p] = ['T' for i in result[p]] 
                else:
                    del result[p]
                    del new_eqs[p]
                    del new_mod[p]
                    p = p - 1
                break
        p = p + 1

    print('no neg cong',new_eqs,new_mod,result)
                    
    for p in range(len(result)):           
        eliminated = False
        for j in range(len(new_eqs[p])):
            if new_mod[p][j] == 0 and new_eqs[p][j][i] != 0 and result[p][j]=='T':
                c = new_eqs[p][j][i]
                for k in range(len(new_eqs[p])):
                    if k != j and new_eqs[p][k][i] != 0:
                        d = new_eqs[p][k][i]
                        for m in range(len(new_eqs[p][k])):
                            new_eqs[p][k][m] = new_eqs[p][k][m]*c - new_eqs[p][j][m]*d
                        new_mod[p][k] = abs(new_mod[p][k] * c)
                new_mod[p][j] = abs(c)
                new_eqs[p][j][i] = 0
                eliminated = True
                break
        if not eliminated:
            for j in range(len(new_eqs[p])):
                if new_eqs[p][j][i] != 0 and result[p][j]=='T':
                       
                    for k in range(len(new_eqs[p])):
                        if k!=j and new_eqs[p][k][i]!=0:
                            if result[p][k]=='F' and new_mod[p][k] == 0:
                                for h in range(len(new_eqs[p][k])):
                                    new_eqs[p][k][h] = 0
                                result[p][k]='T'
                            else:
                                if new_mod[p][k] != 0 and result[p][k]=='F':
                                    print('negated congruence detected 1')
                                new_eqs[p][j],new_mod[p][j],new_eqs[p][k],new_mod[p][k] = solve_two_congruences(i,new_eqs[p][j],new_mod[p][j],new_eqs[p][k],new_mod[p][k])                                    
                    new_mod[p][j] = compute_gcd(new_mod[p][j], new_eqs[p][j][i])
                    new_eqs[p][j][i] = 0
                        
                    eliminated = True
                    
                    break
        if not eliminated:
            for j in range(len(new_eqs[p])):
                if new_eqs[p][j][i] != 0 and result[p][j]=='F':
                    if new_mod[p][j] != 0:
                        print('negated congruence detected 2',p, new_eqs, new_mod,result)
                    for m in range(len(new_eqs[p][j])):
                        new_eqs[p][j][m] = 0
                    result[p][j] = 'T'

    print('solved:',new_eqs, new_mod, result)
    new_matrix = []
    for p in range(len(result)):
        new_conjunct = []
        for k in range(len(new_eqs[p])):
            new_equation = []
            
            if any([c!=0 for c in new_eqs[p][k][:-1]]):
                
                if tuple(new_eqs[p][k]) == (1,0,0):
                    new_eqs[p][k] = (-1,0,0)
                    
                for v in range(len(new_eqs[p][k])):
                    char = chr(ord('a')+v)
                    if v == len(new_eqs[p][k])-1:
                        char = '1'
                    if new_eqs[p][k][v] > 0:
                        for m in range(new_eqs[p][k][v]):
                            new_equation = new_equation + [char,'+']
                new_equation = new_equation + ['0','=' + str(new_mod[p][k])]
                for v in range(len(new_eqs[p][k])):
                    char = chr(ord('a')+v)
                    if v == len(new_eqs[p][k])-1:
                        char = '1'
                    if new_eqs[p][k][v] < 0:
                        for m in range(abs(new_eqs[p][k][v])):
                            new_equation = new_equation + [char,'+']
                new_equation = new_equation + ['0']
            else:
                constant = new_eqs[p][k][-1]
                z = new_mod[p][k]
                if z != 0:
                    constant = constant%z
                if constant == 0:
                    new_equation = ['T']
                else:
                    new_equation = ['F']
            
            if result[p][k] == 'T':
                new_conjunct = new_conjunct + ['&','{'] + new_equation + ['}']
            else:
                new_conjunct = new_conjunct + ['&','~','{'] + new_equation + ['}']
        new_conjunct = new_conjunct[1:]
        new_matrix = new_matrix + ['|','{'] + new_conjunct + ['}']
    new_matrix = new_matrix[1:]


    var = [chr(ord('a')+v) for v in range(len(quan)-1)] # one variable has been eliminated
    print('quan',quan,'new_matrix',''.join(new_matrix))
    if len(quan) > 0:
        if quan[-1] == '~E':
            new_matrix = ['~','{'] + new_matrix + ['}']
    new_matrix = eval_matrix(new_matrix)
    if len(new_matrix) == 1:
        if new_matrix[0] == 'F':
            new_matrix = ['1','=0','0']
        else:
            new_matrix = ['0','=0','0']
            
    return new_matrix, quan[:-1],var

#sentence = "~xE(yA(x+y=y+y+y)):~zA(z+z+z+z=0)"
sentence = "~aA(bE(a+b=a+a)&cA(c+a=a+a))"
#sentence = "~zE(~xA(~yA(x+y=z)))"
#sentence = "xA(x+x=0)&~(yE(y+1+1+1=0)|zA(z+0=1+1+1))"
#sentence,_ = random_sentence()
#print(''.join(sentence))
#sentence = "xA(yE((y+y+y=x)|(y+y+y+1=x)|(y+y+y+1+1=x)))"
#sentence = ['x','A','(','y','E','(','y','+','x','+','1','=3','x',')',')']
#sentence = 'xA(yE(y+1=x)&zE(z=x+x))'
#sentence = '((0=0)&~(1=1+1)&(1=0))'
#sentence = 'xA(yE((y+y=x)|(y+y+1=x)))'
#sentence = 'xA(yE((y+y=x)|(y+y+y=x+x)))'
#sentence = input()

if is_meaningful(sentence):
    sentence = [i for i in sentence]
    print(''.join(sentence), "is meaningful")
    sentence = eliminate_implication(sentence)

    sentence = change_braces(sentence, free_variables(sentence,0,len(sentence)))
    sentence = bracing(sentence)
    print(''.join(sentence))

    while not (all_round_brackets_removed(sentence) and all_quantifiers_on_left(sentence)):
        #old_sentence = sentence
        sentence,caught = eliminate_neg_quantifiers(sentence)
        #print("neg ", ''.join(sentence))
        sentence = bracket_opener(sentence)
        #print("bra ", ''.join(sentence))
        sentence = eliminate_superflous_brackets(sentence)
        print(" => ", ''.join(sentence))
        #if old_sentence == sentence:
        #    break
    mat,quan,var = matrix(sentence)
    #print("mat ", ''.join(mat),quan,var)
    
    result,pos,eqs = disjunctive_normal_form(mat)
    #print(result,pos,[''.join(i) for i in eqs])

    coeff = [None for i in range(len(eqs))]
    mod = [0 for i in range(len(eqs))]
    for i in range(len(eqs)):
        coeff[i], mod[i] = equation(eqs[i],var)
    #print('coeff,mod=',coeff,mod)

    # Now need to combine coeff with result and finally with quan
    new_mat, new_quan, new_var = eliminate_last_variable(coeff,result,mod,quan)
    print(''.join(new_mat), new_quan, new_var)
    while len(new_var) != 0:
        result,pos,eqs = disjunctive_normal_form(new_mat)
        print('result:',result)
        coeff = [None for i in range(len(eqs))]
        mod = [0 for i in range(len(eqs))]
        for i in range(len(eqs)):
            coeff[i], mod[i] = equation(eqs[i],new_var)
        new_mat, new_quan, new_var = eliminate_last_variable(coeff,result,mod,new_quan)
        print('asd',''.join(new_mat), new_quan, new_var)
        
    matrix,pos,eqs = get_conjuncts(new_mat)
    for e in range(len(eqs)):
        coeff,mod = equation(eqs[e],[])
        if mod is None:
            print(eqs[e])
        if mod == 0:
            if coeff[-1] == 0:
                matrix[pos[e]] = 'T'
            else:
                matrix[pos[e]] = 'F'
        else:
            if coeff[-1]%mod == 0:
                matrix[pos[e]] = 'T'
            else:
                matrix[pos[e]] = 'F'
    print(eval_bool(matrix))
else:
    print("Not meaningful")

        
    

    
