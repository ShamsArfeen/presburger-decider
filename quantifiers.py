
def free_variables(sentence,start_,end_,recursive=True):
    quantifier = -1
    free_var = dict()
    bracket_depth = 0
    end = -1
    variable = ''
    for i in range(start_,end_):
        if quantifier == -1 and (sentence[i] == 'A' or sentence[i] == 'E'):
            variable = sentence[i-1]
            start = i+1
            quantifier = bracket_depth
            continue
        if sentence[i] in ['(','{']:
            bracket_depth += 1
        elif sentence[i] in [')','}']:
            bracket_depth -= 1
        if bracket_depth == quantifier:
            end = i+1
            quantifier = -1
            if recursive:
                nested_var = free_variables(sentence,start+1,i)
                free_var[variable] = (start,end,nested_var)
            else:
                free_var[variable] = (start,end)
    return free_var


def other_quant(quant):
    if quant == 'E':
        return 'A'
    return 'E'

def eliminate_negated_quantifiers(sentence):
    variables = free_variables(sentence,0,len(sentence),False)
    quantifiers = sorted([(pos,var) for var,pos in variables.items()])[::-1]
    caught = False
    for pos,var in quantifiers:
        start,end = pos
        if start >= 3:
            if sentence[start-3]=='~':
                caught = True
                nested, _ = eliminate_negated_quantifiers(sentence[start:end])
                sentence = sentence[:start-3] + [var] + [other_quant(sentence[start-1])] + ['{','~','('] + nested[1:-1] + [')','}'] + sentence[end:]
                continue
        nested, excaught = eliminate_negated_quantifiers(sentence[start:end])
        sentence = sentence[:start] + nested + sentence[end:]
        if excaught:
            caught = True
    return sentence, caught

def curlify_quantifiers(sentence,variables):
    for var,pos in variables.items():
        start,end,nested = pos
        sentence[start] = '{'
        sentence[end-1] = '}'
        sentence = curlify_quantifiers(sentence,nested)
    return sentence

def insert_round_brackets(sentence):
    i = 0
    while i < len(sentence):
        if sentence[i] == '{':
            sentence = sentence[:i] + ['{','('] + sentence[i+1:]
            i += 1
        if sentence[i] == '}':
            sentence = sentence[:i] + [')','}'] + sentence[i+1:]
            i += 1
        i += 1
    return sentence

def introduce_brackets(sentence):
    variables = free_variables(sentence, 0, len(sentence))
    sentence = curlify_quantifiers(sentence, variables)
    sentence = insert_round_brackets(sentence)
    return sentence

def free_innermost_round_bracket(sentence):
    maximum_depth = 0
    start = -1
    end = len(sentence)+1
    bracket_depth = 0
    for i in range(0,len(sentence)):
        if sentence[i] == '(':
            bracket_depth += 1
            if maximum_depth <= bracket_depth:
                maximum_depth = bracket_depth
                start = i
        elif sentence[i] == ')':
            if bracket_depth == maximum_depth:
                end = i+1
            bracket_depth -= 1

    flag = False
    for i in range(start,end-1):
        if sentence[i] in ['E','A']:
            flag = True
    if flag == False:
        sentence[start] = '{'
        sentence[end-1] = '}'
        return sentence

    prefix = []
    suffix = []
    for i in range(start+1,end-1):
        if sentence[i] in ['E','A']:
            prefix = prefix + sentence[i-1:i+1] + ['{']
            suffix = ['}'] + suffix
    sub_sentence = sentence[start+1:end-1]
    i = 0
    while i < len(sub_sentence):
        if sub_sentence[i] in ['E','A']:
            sub_sentence = sub_sentence[:i-1] + sub_sentence[i+1:]
            i -= 2
        i += 1
    if start == -1:
        return prefix + sub_sentence + suffix
    sentence = sentence[:start] + prefix + sub_sentence + suffix + sentence[end:]
    return sentence

def all_quantifiers_on_left(sentence):
    index = 1
    for i in range(1,len(sentence),3):
        index = i
        if sentence[i] not in ['A','E']:
            break
    for i in range(index, len(sentence)):
        if sentence[i] in ['A','E']:
            return False
    return True   
