from prenix_form import prenix_form

def write_matrix(M):
    sentence = ""
    for symbol in M:
        if symbol[0] == '=':
            if symbol[1] != '0':
                symbol = '=(mod' + symbol[1:] + ')'
            else:
                symbol = '='
        sentence += symbol
    return sentence

def print_matrix(M,Q,V):
    sentence = ""
    for q in range(len(Q)):
        if Q[q] == '~E':
            sentence += '~' + V[q] + 'E{'
        else:
            sentence += V[q] + 'E{'
    for symbol in M:
        if symbol[0] == '=':
            if symbol[1] != '0':
                symbol = '=(mod' + symbol[1:] + ')'
            else:
                symbol = '='
        sentence += symbol
    for q in range(len(Q)):
        sentence += '}'
    print('=>', sentence)

def matrix(sentence):
    index = -2
    quans = []
    variables = []
    for i in range(1,len(sentence),3):
        if sentence[i] not in ['A','E']:
            break
        quans.append(sentence[i])
        variables.append(sentence[i-1])
        index = i
    M = sentence[index+2:len(sentence) - (index+2)//3]
    carry_neg = False
    for i in range(len(quans)):
        if quans[i] == 'A' and not carry_neg:
            quans[i] = '~E'
            carry_neg = True
        elif quans[i] == 'E' and carry_neg:
            quans[i] = '~E'
            carry_neg = False
        elif quans[i] == 'A' and carry_neg:
            quans[i] = 'E'
            carry_neg = True
    if carry_neg:
        M = ['~','{'] + M + ['}']
    return M, quans, variables


def get_conjuncts(matrix):
    pos = []
    eqs = []
    i = 0
    while i < len(matrix):
        if matrix[i] in ['0','1'] or matrix[i].islower():
            pos.append(i)
            for j in range(i,len(matrix)):
                if matrix[j] == '}':
                    break
                if j == len(matrix)-1:
                    j = len(matrix)
            eqs = eqs + [matrix[i:j]]
            matrix = matrix[:i+1] + matrix[j:]
            matrix[i] = 'V'
        i += 1
    return matrix, pos, eqs

def eval_bool(sentence):
    sentence = ''.join(sentence)
    while len(sentence) > 1:
        #print(sentence)
        k = sentence.find('{F}')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('{T}')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
            
        k = sentence.find('~F')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+2:]
        k = sentence.find('~T')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+2:]
            
        k = sentence.find('F&T')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('T&F')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('F&F')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('T&T')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
            
            
        k = sentence.find('F|T')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
        k = sentence.find('T|F')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
        k = sentence.find('F|F')
        if k!=-1:
            sentence = sentence[:k] + 'F' + sentence[k+3:]
        k = sentence.find('T|T')
        if k!=-1:
            sentence = sentence[:k] + 'T' + sentence[k+3:]
    return sentence[0]

def combinations(n):
    if n > 10:
        print('overflow 2^',n)
        exit()
    if n == 0:
        return [[]]
    else:
        ilist = []
        for i in combinations(n-1):
            ilist = ilist + [i+['F'],i+['T']]
        return ilist

def disjunctive_normal_form(matrix):
    matrix,pos,eqs = get_conjuncts(matrix)
    n = len(pos)
    result = []

    same = dict()
    for e in range(len(eqs)):
        u = tuple(eqs[e])
        same.setdefault(u,[]).append(pos[e])
    n = len(same.keys())

    for truths in combinations(n):
        m = matrix

        pos2 = list(same.values())
        for b in range(len(pos2)):
            for bb in pos2[b]:
                m[bb] = truths[b]
                  
        ans = eval_bool(m)
        if ans == 'T':
            result.append(truths)
            
    return result,[list(v) for v in same.keys()]

def scan_coefficients(sentence,var):
    coeff = [0 for v in range(len(var)+1)]
    mod = None
    for v in range(len(var)):
        sign = 1
        for i in range(len(sentence)):
            if sentence[i] == var[v]:
                coeff[v] += sign
            elif sentence[i][0] == '=':
                sign = -1
                if len(sentence[i]) == 1:
                    mod = 0
                else:
                    mod = int(sentence[i][1:])
    sign = 1
    for i in range(len(sentence)):
        if sentence[i] == '1':
            coeff[-1] += sign
        if sentence[i][0] == '=':
            sign = -1
            if len(sentence[i]) == 1:
                mod = 0
            else:
                mod = int(sentence[i][1:])
    return coeff,mod

def extract_matrix(sentence):
    sentence = prenix_form(sentence)
    M,Q,V = matrix(sentence)
    matrix_expression,ground_statements = disjunctive_normal_form(M)

    # print([''.join(statement) for statement in ground_statements])
    coefficients = []
    moduli = []
    for statement in ground_statements:
        c,m = scan_coefficients(statement, V)
        coefficients.append(c)
        moduli.append(m)
    
    return coefficients,moduli,matrix_expression,V,Q

'''
sentence = "xA(x+x=0)&~(yE(y+1+1+1=0)|zA(z+0=1+1+1))"
sentence = [i for i in sentence]
coefficients,moduli,matrix_expression,V,Q = extract_matrix(sentence)
print('coefficients:', coefficients)
print('moduli:', moduli)
print('matrix_expression:', matrix_expression)
print('V:', V)
print('Q:', Q)
'''
