def syntax_ok(sentence, free_variable=[]):
    if '(' not in sentence:
        if len(sentence) == 1:
            print('error 0')
            return sentence[0] in ['F','T']
        has_equation_sign = False
        for i in range(len(sentence)):
            if i%2 == 1:
                if sentence[i][0] not in ['+','=']:
                    print('error 1')
                    return False
                if sentence[i][0] == '=' and has_equation_sign == True:
                    print('error 2')
                    return False
                elif sentence[i][0] == '=':
                    has_equation_sign = True
            elif sentence[i] not in (free_variable + ['0','1']):
                print('error 3', sentence)
                return False
        return True
    
    # Scan all brackets at outermost level
    bracket_depth = 0
    positions_left = []
    positions_right = []
    for i in range(len(sentence)):
        if sentence[i] == '(':
            bracket_depth += 1
            if bracket_depth == 1:
                positions_left.append(i)
        elif sentence[i] == ')':
            bracket_depth -= 1
            if bracket_depth == 0:
                positions_right.append(i)
        if bracket_depth == 0 and sentence[i].islower() and sentence[i].isalpha() and sentence[i] not in free_variable:
            if i < len(sentence)-1:
                if sentence[i+1] not in ['E','A']:
                    print('error 17')
                    return False
            else:
                print('error 4')
                return False
        if bracket_depth < 0:
            print('error 5')
            return False
    if bracket_depth != 0:
        print('error 6')
        return False
    
    # Recursively check syntax inside each bracket of outermost level
    for i in range(len(positions_left)):
        left_bracket = positions_left[i]
        right_bracket = positions_right[i]
        syntax_subsentence_ok = False
        if left_bracket > 0:
            if sentence[left_bracket-1] in ['A','E']:
                if left_bracket > 1:
                    current_variable = sentence[left_bracket - 2]
                    if current_variable in free_variable or not current_variable.isalpha() or not current_variable.islower():
                        print('error 7')
                        return False
                    new_var = free_variable + [current_variable]
                    positions_left[i] = left_bracket - 2
                    syntax_subsentence_ok = True
                    if not syntax_ok(sentence[left_bracket+1:right_bracket],new_var):
                        print('error 8')
                        return False
                else:
                    print('error 9')
                    return False
        if not syntax_subsentence_ok:
            if not syntax_ok(sentence[left_bracket+1:right_bracket],free_variable):
                print('error 10')
                return False
        
    left_bracket = positions_left[0]
    if left_bracket == 1:
        if sentence[0] != '~':
            print('error 11')
            return False
    elif left_bracket != 0:
        print('error 12')
        return False
    operator = ''
    for i in range(len(positions_left)-1):
        end_ = positions_right[i]
        start_ = positions_left[i+1]
        if start_ - end_ == 2:
            if sentence[end_+1] not in ['|','&',':'] or (operator != '' and operator != sentence[end_+1]):
                print('error 13')
                return False
            operator = sentence[end_+1]
        elif start_ - end_ == 3:
            if sentence[end_+1] not in ['|','&',':'] or sentence[end_+2] != '~' or (operator != '' and operator != sentence[end_+1]):
                print('error 14')
                return False
            operator = sentence[end_+1]
        else:
            print('error 15')
            return False
    if not positions_right[-1] == len(sentence)-1:
        print('error 16', sentence)
        return False
    return True


'''
sentence = "~zE(z=0)&~xA(x=x)"
if syntax_ok(sentence):
    print(sentence, "is meaningful")
'''

