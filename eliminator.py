from helper import *
from matrix import write_matrix

def eval_matrix(sentence):
    while len(sentence) > 1 and ('T' in sentence or 'F' in sentence):
        print('\t=>',write_matrix(sentence))

        k=0
        while k != -1:
            k = find_list(sentence, '{F}')
            if k!=-1:
                sentence = sentence[:k] + ['F'] + sentence[k+3:]
                
        k=0
        while k != -1:
            k = find_list(sentence, '{T}')
            if k!=-1:
                sentence = sentence[:k] + ['T'] + sentence[k+3:]

        caught = True
        while caught:
            caught = False
            k = find_list(sentence, '~F')
            if k!=-1:
                caught = True
                sentence = sentence[:k] + ['T'] + sentence[k+2:]
            k = find_list(sentence, '~T')
            if k!=-1:
                caught = True
                sentence = sentence[:k] + ['F'] + sentence[k+2:]
            
        k = find_list(sentence, 'F&')
        if k!=-1:
            if sentence[k+2] in ['T','F']:
                j = k+2
            else:
                bracket = 0
                i = k+2
                if sentence[k+2] == '~':
                    i=k+3
                for s in range(i,len(sentence)):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
            sentence = sentence[:k] + ['F'] + sentence[j+1:]
            
        k = find_list(sentence, '&F')
        if k!=-1:
            if sentence[k-1] in ['T','F']:
                j = k-1
            else:
                bracket = 0
                for s in range(k-1,-1,-1):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
                if j>0:
                    if sentence[j-1] == '~':
                        j -= 1
            sentence = sentence[:j] + ['F'] + sentence[k+2:]
            
        k = find_list(sentence, '&T')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
        k = find_list(sentence, 'T&')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
            

            
        k = find_list(sentence, 'T|')
        if k!=-1:
            if sentence[k+2] in ['T','F']:
                j = k+2
            else:
                bracket = 0
                i = k+2
                if sentence[k+2] == '~':
                    i=k+3
                for s in range(i,len(sentence)):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
            sentence = sentence[:k] + ['T'] + sentence[j+1:]
            
        k = find_list(sentence, '|T')
        if k!=-1:
            if sentence[k-1] in ['T','F']:
                j = k-1
            else:
                bracket = 0
                for s in range(k-1,-1,-1):
                    j = s
                    if sentence[s] == '{':
                        bracket += 1
                    elif sentence[s] == '}':
                        bracket -= 1
                    if bracket == 0:
                        break
                if j>0:
                    if sentence[j-1] == '~':
                        j -= 1
            sentence = sentence[:j] + ['T'] + sentence[k+2:]
            
        k = find_list(sentence, '|F')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
        k = find_list(sentence, 'F|')
        if k!=-1:
            sentence = sentence[:k] + sentence[k+2:]
            
    return sentence

def eliminate_last_variable(eqs,mod, result, quan,V):
    if len(result) == 0:
        if len(quan) > 0:
            if quan[-1] == '~E':
                return ['0','=0','0'],quan[:-1], V[:-1]
            else:
                return ['1','=0','0'],quan[:-1], V[:-1]
        else:
            return ['1','=0','0'], [], []
        
    i = len(quan)-1 # last variable
    new_eqs = [[e.copy() for e in eqs] for p in range(len(result))]
    new_mod = [mod.copy() for p in range(len(result))]
    
    p = 0
    while p < len(result):
        for r in range(len(result[p])):
            if result[p][r] == 'F' and new_mod[p][r]!=0:

                result[p][r] = 'T'
                for m in range(1,new_mod[p][r]):    
                    false_set = [e.copy() for e in new_eqs[p]]
                    false_set[r][-1] += m
                    false_set[r][-1] = false_set[r][-1]%new_mod[p][r]

                    new_eqs = new_eqs + [false_set]
                    new_mod = new_mod + [new_mod[p].copy()]
                    result = result + [result[p].copy()]
                    
                if new_mod[p][r] == 1:
                    new_eqs[p][r] = [0 for i in new_eqs[p][r]]
                    new_eqs[p][r][-1] = 1
                    new_mod[p][r] = 0
                    result[p] = ['T' for i in result[p]] 
                else:
                    del result[p]
                    del new_eqs[p]
                    del new_mod[p]
                    p = p - 1
                break
        p = p + 1
                    
    for p in range(len(result)):           
        eliminated = False
        for j in range(len(new_eqs[p])):
            if new_mod[p][j] == 0 and new_eqs[p][j][i] != 0 and result[p][j]=='T':
                c = new_eqs[p][j][i]
                for k in range(len(new_eqs[p])):
                    if k != j and new_eqs[p][k][i] != 0:
                        d = new_eqs[p][k][i]
                        for m in range(len(new_eqs[p][k])):
                            new_eqs[p][k][m] = new_eqs[p][k][m]*c - new_eqs[p][j][m]*d
                        new_mod[p][k] = abs(new_mod[p][k] * c)
                new_mod[p][j] = abs(c)
                new_eqs[p][j][i] = 0
                eliminated = True
                break
        if not eliminated:
            for j in range(len(new_eqs[p])):
                if new_eqs[p][j][i] != 0 and result[p][j]=='T':
                       
                    for k in range(len(new_eqs[p])):
                        if k!=j and new_eqs[p][k][i]!=0:
                            if result[p][k]=='F' and new_mod[p][k] == 0:
                                for h in range(len(new_eqs[p][k])):
                                    new_eqs[p][k][h] = 0
                                result[p][k]='T'
                            else:
                                if new_mod[p][k] != 0 and result[p][k]=='F':
                                    print('negated congruence detected 1')
                                new_eqs[p][j],new_mod[p][j],new_eqs[p][k],new_mod[p][k] = solve_two_congruences(i,new_eqs[p][j],new_mod[p][j],new_eqs[p][k],new_mod[p][k])                                    
                    new_mod[p][j] = compute_gcd(new_mod[p][j], new_eqs[p][j][i])
                    new_eqs[p][j][i] = 0
                        
                    eliminated = True
                    
                    break
        if not eliminated:
            for j in range(len(new_eqs[p])):
                if new_eqs[p][j][i] != 0 and result[p][j]=='F':
                    if new_mod[p][j] != 0:
                        print('negated congruence detected 2',p, new_eqs, new_mod,result)
                    for m in range(len(new_eqs[p][j])):
                        new_eqs[p][j][m] = 0
                    result[p][j] = 'T'

    #print('solved:',new_eqs, new_mod, result)
    new_matrix = []
    for p in range(len(result)):
        new_conjunct = []
        for k in range(len(new_eqs[p])):
            new_equation = []
            
            if any([c!=0 for c in new_eqs[p][k][:-1]]):                    
                for v in range(len(new_eqs[p][k])):
                    if v == len(new_eqs[p][k])-1:
                        char = '1'
                    else:
                        char = V[v]
                    if new_eqs[p][k][v] > 0:
                        for m in range(new_eqs[p][k][v]):
                            new_equation = new_equation + [char,'+']
                new_equation = new_equation + ['0','=' + str(new_mod[p][k])]
                for v in range(len(new_eqs[p][k])):
                    if v == len(new_eqs[p][k])-1:
                        char = '1'
                    else:
                        char = V[v]
                    if new_eqs[p][k][v] < 0:
                        for m in range(abs(new_eqs[p][k][v])):
                            new_equation = new_equation + [char,'+']
                new_equation = new_equation + ['0']
            else:
                constant = new_eqs[p][k][-1]
                z = new_mod[p][k]
                if z != 0:
                    constant = constant%z
                if constant == 0:
                    new_equation = ['T']
                else:
                    new_equation = ['F']
            
            if result[p][k] == 'T':
                new_conjunct = new_conjunct + ['&','{'] + new_equation + ['}']
            else:
                new_conjunct = new_conjunct + ['&','~','{'] + new_equation + ['}']
        new_conjunct = new_conjunct[1:]
        new_matrix = new_matrix + ['|','{'] + new_conjunct + ['}']
    new_matrix = new_matrix[1:]


    var = [V[v] for v in range(len(quan)-1)] # one variable has been eliminated
    #print('quan',quan,'new_matrix',''.join(new_matrix))
    if len(quan) > 0:
        if quan[-1] == '~E':
            new_matrix = ['~','{'] + new_matrix + ['}']    
    
    new_matrix = eval_matrix(new_matrix)
    if len(new_matrix) == 1:
        if new_matrix[0] == 'F':
            new_matrix = ['1','=0','0']
        else:
            new_matrix = ['0','=0','0']
            
    return new_matrix, quan[:-1],var