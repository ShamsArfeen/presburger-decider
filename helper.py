def compute_gcd(x, y):
    if x == 0:
        return y
    if y == 0:
        return x
    if x == 1 or y == 1:
        return 1
    if x > y:
        return compute_gcd(x % y, y)
    return compute_gcd(x, y % x)
    
def compute_lcm(x, y):
   if x > y:
       greater = x
   else:
       greater = y
   while(True):
       if((greater % x == 0) and (greater % y == 0)):
           lcm = greater
           break
       greater += 1
   return lcm

def sgn(n):
    if n > 0:
        return 1
    elif n < 0:
        return -1
    else:
        print('error: sign of zero')
        return None

def solve_two_congruences(v,eqj,modj,eqk,modk):
    onej = sgn(eqj[v])
    onek = sgn(eqk[v])
    lcm = compute_lcm(modj,modk)
    for i in range(len(eqj)):
        eqj[i] = onej * eqj[i] * abs(lcm//modj)
        eqk[i] = onek * eqk[i] * abs(lcm//modk)
    modj = lcm
    modk = lcm
    while eqj[v] != eqk[v]:
        if eqj[v] > eqk[v]:
            for i in range(len(eqj)):
                eqj[i] = eqj[i] - eqk[i]
        else:
            for i in range(len(eqj)):
                eqk[i] = eqk[i] - eqj[i]
    for i in range(len(eqj)):
        eqk[i] = eqk[i] - eqj[i]
    return eqj,modj,eqk,modk

def find_list(sentence,sub):
    sub = [i for i in sub]
    for j in range(len(sentence) - len(sub) + 1):
        if all([sentence[j+k] == sub[k] for k in range(len(sub))]):
            return j
    return -1