from quantifiers import all_quantifiers_on_left, free_innermost_round_bracket, introduce_brackets, eliminate_negated_quantifiers

def all_round_brackets_removed(sentence):
    if '(' not in sentence:
        return True
    return False

def eliminate_superflous_brackets(sentence):
    bracket_depth = 0
    left_bracket = []
    right_bracket = []
    stack = []
    for i in range(len(sentence)):
        if sentence[i] == '{':
            bracket_depth += 1
            stack.append(i)
        elif sentence[i] == '}':
            bracket_depth -= 1
            left_bracket.append(stack.pop())
            right_bracket.append(i)
    deletions = []
    for i in range(len(left_bracket)-1):
        if right_bracket[i+1] == right_bracket[i]+1 and left_bracket[i+1]+1 == left_bracket[i]:
            deletions = deletions + [left_bracket[i],right_bracket[i]]
    new_sentence = []
    for i in range(len(sentence)):
        if i not in deletions:
            new_sentence.append(sentence[i])
    return new_sentence

def prenix_form(sentence):
    sentence = introduce_brackets(sentence)

    while not (all_round_brackets_removed(sentence) and all_quantifiers_on_left(sentence)):
        sentence, _ = eliminate_negated_quantifiers(sentence)
        sentence = free_innermost_round_bracket(sentence)
        sentence = eliminate_superflous_brackets(sentence)
        print("=>", "".join(sentence))
    
    return sentence

'''
sentence = "xA(x+x=0)&~(yE(y+1+1+1=0)|zA(z+0=1+1+1))"
sentence = [i for i in sentence]
prenix = prenix_form(sentence)
print(''.join(sentence))
print(''.join(prenix))
'''