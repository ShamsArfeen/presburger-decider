from eliminator import *
from matrix import *
from syntax import *

def prover(sentence):
    coefficients,moduli,matrix_expression,V,Q = extract_matrix(sentence)
    M, Q, V = eliminate_last_variable(coefficients, moduli, matrix_expression, Q, V)
    print_matrix(M, Q, V)

    while len(V) != 0:
        matrix_expression,ground_statements = disjunctive_normal_form(M)
        coefficients = []
        moduli = []
        for statement in ground_statements:
            c,m = scan_coefficients(statement, V)
            coefficients.append(c)
            moduli.append(m)
        M, Q, V = eliminate_last_variable(coefficients, moduli, matrix_expression, Q, V)
        print_matrix(M, Q, V)

    M,pos,eqs = get_conjuncts(M)
    for e in range(len(eqs)):
        coeff,mod = scan_coefficients(eqs[e],[])
        if mod is None:
            print(eqs[e])
        if mod == 0:
            if coeff[-1] == 0:
                M[pos[e]] = 'T'
            else:
                M[pos[e]] = 'F'
        else:
            if coeff[-1]%mod == 0:
                M[pos[e]] = 'T'
            else:
                M[pos[e]] = 'F'
    return eval_bool(M)


print('''
============= PRESBURGER ARITHMETIC =============
NOTATION:
      ~ logical not
      | logical or
      & logical and
      0,1 constants (zero, one)
      +,= operators (addition, equality)
      A,E quantifiers (for all, there exists)

AXIOMS:
      xA(x=x)
      xA(yA(x+y=y+x))
      xA(x+0=x)
      xA(yA(zA(((x+z=y+z)&(x=y))|~(x=y))))
      xA(yA(zA(((x=y)&(y=z)&(x=z))|~((x=y)&(y=z)))))

AXIOM SCHEMA:
      xA(yA(((x+x+x...=y+y+y+...)&(x=y))|~(x=y)))
      ~xE(x+x+x...=1)
      ALL PROPOSITIONAL TAUTOLOGIES
=================================================
''')

while True:
    sentence = input(">>> ")
    if syntax_ok(sentence):
        print()
        sentence = [i for i in sentence]
        print(prover(sentence))
    else:
        print("Syntax error:", sentence)
