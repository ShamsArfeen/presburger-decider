# Presburger Decider

## Notations:
      ~ logical not
      | logical or
      & logical and
      0,1 constants (zero, one)
      +,= operators (addition, equality)
      A,E quantifiers (for all, there exists)

## Axioms:
      xA(x=x)
      xA(yA(x+y=y+x))
      xA(x+0=x)
      xA(yA(zA(((x+z=y+z)&(x=y))|~(x=y))))
      xA(yA(zA(((x=y)&(y=z)&(x=z))|~((x=y)&(y=z)))))

## Axiom Schema:
      xA(yA(((x+x+x...=y+y+y+...)&(x=y))|~(x=y)))
      ~xE(x+x+x...=1)
      All Propositionsl Tautologies

## Examples

```
terminal> py PA.py
>>> xA(yE((y+y=x)|(y+y+y=x+x)))

=> xA{(yE{((y+y=x)|{y+y+y=x+x})})}
=> xA{(yE{({y+y=x}|{y+y+y=x+x})})}
=> xA{(yE{{y+y=x}|{y+y+y=x+x}})}
=> xA{yE{{y+y=x}|{y+y+y=x+x}}}
=> ~xE{~{{~{x+0=0}&{0=(mod3)x+x+0}}|{{0=(mod2)x+0}&~{0=x+0}}|{{0=(mod2)x+0}&{0=x+0}}}}
        => ~{{{T}&{T}&{F}&~{T}}|{{T}&{T}&{F}&{T}}|{{T}&{T}&{T}&{T}}|{{T}&{T}&{T}&{T}}|{~{T}&{F}&{F}&{T}}|{~{T}&{F}&{F}&{T}}|{{T}&{F}&{F}&~{T}}|{{T}&{F}&{F}&~{T}}|{{T}&{F}&{F}&{T}}|{{T}&{F}&{F}&{T}}}
        => ~{{F}|{T&F&T}|{T&T&T&T}|{T&T&T&T}|{F&F&F&T}|{F&F&F&T}|{T&F&F&F}|{T&F&F&F}|{T&F&F&T}|{T&F&F&T}}        
        => ~{{F}|{T&T}|{T&T&T&T}|{F&F&F&T}|{F&F&F&T}|{T&F&F&F}|{T&F&F&F}|{T&F&F&T}|{T&F&F&T}}
        => ~{{T}|{T&T&T}|{F&T}|{F&F&F&T}|{T&F&F&F}|{T&F&F&F}|{T&F&F&T}|{T&F&F&T}}
        => ~{T|{F}|{F&F&T}|{T&F&F&F}|{T&F&F&F}|{T&F&F&T}|{T&F&F&T}}
        => ~{T|{F}|{F&F&F}|{F&F&F}|{T&F&F&T}|{T&F&F&T}}
        => ~{T|{F}|{F&F&F}|{F&F}|{T&F&F&T}}
        => ~{T|{F}|{F&F}|{F&F}}
        => ~{T|{F}|{F}}
        => ~{T}
=> 1=0
F
>>> xA(yE(y+1=x)&zE(z=x+x))

=> xA{(yE{(y+1=x)}&zE{z=x+x})}
=> xA{(yE{y+1=x}&zE{z=x+x})}
=> xA{yE{zE{{y+1=x}&{z=x+x}}}}
=> ~xE{~yE{{{y+1+0=x+0}&{0=(mod1)x+x+0}}}}
=> ~xE{~{{{1+0=(mod1)x+0}&{0=(mod1)x+x+0}}}}
        => ~{{{F}&{T}}|{{F}&{T}}|{{T}&{F}}}
        => ~{{F}|{F}|{F}}
        => ~{F}
=> 0=0
T
>>> ((0=0)&~(1=1+1)&(1=0))

=> ((0=0)&~(1=1+1)&{1=0})
=> ((0=0)&~{1=1+1}&{1=0})
=> ({0=0}&~{1=1+1}&{1=0})
=> {{0=0}&~{1=1+1}&{1=0}}
        => {{T}&~{T}&{T}}
        => {F}
=> 1=0
F
>>> xA(yE((y+y=x)|(y+y+1=x)))

=> xA{(yE{((y+y=x)|{y+y+1=x})})}
=> xA{(yE{({y+y=x}|{y+y+1=x})})}
=> xA{(yE{{y+y=x}|{y+y+1=x}})}
=> xA{yE{{y+y=x}|{y+y+1=x}}}
        => ~{{~{F}&{1+0=(mod2)x+0}}|{{0=(mod2)x+0}&~{F}}|{{0=(mod2)x+0}&{F}}}
        => ~{{{1+0=(mod2)x+0}}|{{0=(mod2)x+0}}|{F}}
=> ~xE{~{{{1+0=(mod2)x+0}}|{{0=(mod2)x+0}}}}
        => ~{{{T}&{F}}}
        => ~{{F}}
=> 0=0
T
>>> xA(yE((y+y+y=x)|(y+y+y+1=x)|(y+y+y+1+1=x)))

=> xA{(yE{((y+y+y=x)|(y+y+y+1=x)|{y+y+y+1+1=x})})}
=> xA{(yE{((y+y+y=x)|{y+y+y+1=x}|{y+y+y+1+1=x})})}
=> xA{(yE{({y+y+y=x}|{y+y+y+1=x}|{y+y+y+1+1=x})})}
=> xA{(yE{{y+y+y=x}|{y+y+y+1=x}|{y+y+y+1+1=x}})}
=> xA{yE{{y+y+y=x}|{y+y+y+1=x}|{y+y+y+1+1=x}}}
        => ~{{~{F}&~{F}&{1+1+0=(mod3)x+0}}|{~{F}&{1+0=(mod3)x+0}&~{F}}|{~{F}&{1+0=(mod3)x+0}&{F}}|{{0=(mod3)x+0}&~{F}&~{F}}|{{0=(mod3)x+0}&~{F}&{F}}|{{0=(mod3)x+0}&{F}&~{F}}|{{0=(mod3)x+0}&{F}&{F}}}
        => ~{{{1+1+0=(mod3)x+0}}|{T&{1+0=(mod3)x+0}&T}|{T&F}|{{0=(mod3)x+0}&T&T}|{{0=(mod3)x+0}&T&F}|{{0=(mod3)x+0}&F}|{{0=(mod3)x+0}&F&F}}
        => ~{{{1+1+0=(mod3)x+0}}|{{1+0=(mod3)x+0}}|{F}|{{0=(mod3)x+0}&T&T}|{{0=(mod3)x+0}&T&F}|{{0=(mod3)x+0}&F}|{{0=(mod3)x+0}&F}}
        => ~{{{1+1+0=(mod3)x+0}}|{{1+0=(mod3)x+0}}|{{0=(mod3)x+0}&T}|{{0=(mod3)x+0}&F}|{{0=(mod3)x+0}&F}|{{0=(mod3)x+0}&F}}
        => ~{{{1+1+0=(mod3)x+0}}|{{1+0=(mod3)x+0}}|{{0=(mod3)x+0}}|{F}|{{0=(mod3)x+0}&F}|{{0=(mod3)x+0}&F}}      
        => ~{{{1+1+0=(mod3)x+0}}|{{1+0=(mod3)x+0}}|{{0=(mod3)x+0}}|{F}|{{0=(mod3)x+0}&F}}
        => ~{{{1+1+0=(mod3)x+0}}|{{1+0=(mod3)x+0}}|{{0=(mod3)x+0}}|{F}}
=> ~xE{~{{{1+1+0=(mod3)x+0}}|{{1+0=(mod3)x+0}}|{{0=(mod3)x+0}}}}
        => ~{{{T}&{F}&{F}}|{{T}&{F}&{F}}|{{T}&{T}&{F}}|{{T}&{T}&{F}}|{{T}&{F}&{T}}|{{T}&{F}&{F}}|{{T}&{F}&{T}}|{{T}&{F}&{F}}}
        => ~{{F}|{F&F}|{T&F}|{T&T&F}|{T&F&T}|{T&F&F}|{T&F&T}|{T&F&F}}
        => ~{{F}|{F}|{F}|{T&F&T}|{T&F&F}|{T&F&T}|{T&F&F}}
        => ~{F|{F}|{F&F}|{T&F}|{T&F&F}}
        => ~{{F}|{F}|{F&F}}
        => ~{{F}}
=> 0=0
T
```
